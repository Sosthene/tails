# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2020-06-02 16:38+0200\n"
"PO-Revision-Date: 2020-08-14 11:12+0000\n"
"Last-Translator: emmapeel <emma.peel@riseup.net>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: fr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n > 1;\n"
"X-Generator: Weblate 3.5.1\n"

#. type: Plain text
#, no-wrap
msgid "[[!meta title=\"Tails 4.7 is out\"]]\n"
msgstr ""

#. type: Plain text
#, no-wrap
msgid "[[!meta date=\"Tue, 02 Jun 2020 18:00:00 +0000\"]]\n"
msgstr ""

#. type: Plain text
#, no-wrap
msgid "[[!pagetemplate template=\"news.tmpl\"]]\n"
msgstr "[[!pagetemplate template=\"news.tmpl\"]]\n"

#. type: Plain text
#, no-wrap
msgid "[[!tag announce]]\n"
msgstr "[[!tag announce]]\n"

#. type: Plain text
msgid ""
"This release fixes [[many security vulnerabilities|security/"
"Numerous_security_holes_in_4.6]]. You should upgrade as soon as possible."
msgstr ""

#. type: Plain text
#, no-wrap
msgid "[[!toc levels=1]]\n"
msgstr "[[!toc levels=1]]\n"

#. type: Title #
#, no-wrap
msgid "Changes and updates"
msgstr "Changements et mises à jour"

#. type: Plain text
msgid ""
"- Update *Tor Browser* to [9.5](https://blog.torproject.org/new-release-tor-"
"browser-95)."
msgstr ""

#. type: Plain text
msgid ""
"- Update *Thunderbird* to [68.8.0](https://www.thunderbird.net/en-US/"
"thunderbird/68.8.0/releasenotes/)."
msgstr ""

#. type: Title #
#, no-wrap
msgid "Fixed problems"
msgstr "Problèmes corrigés"

#. type: Plain text
msgid ""
"- Make the installation of Additional Software more robust. ([[!tails_ticket "
"17278]])"
msgstr ""

#. type: Bullet: '- '
msgid ""
"Clarify the error message when entering an incorrect password to unlock a "
"VeraCrypt volume: *Wrong passphrase or parameters* instead of *Error "
"unlocking volume*. ([[!tails_ticket 17668]])"
msgstr ""

#. type: Plain text
msgid ""
"- Clean up confusing comments in `/etc/tor/torrc`. ([[!tails_ticket 17706]])"
msgstr ""

#. type: Plain text
msgid ""
"For more details, read our [[!tails_gitweb debian/changelog desc=\"changelog"
"\"]]."
msgstr ""
"Pour plus de détails, lisez notre [[!tails_gitweb debian/changelog desc="
"\"liste des changements\"]]."

#. type: Plain text
#, no-wrap
msgid "<a id=\"known-issues\"></a>\n"
msgstr "<a id=\"known-issues\"></a>\n"

#. type: Title #
#, no-wrap
msgid "Known issues"
msgstr "Problèmes connus"

#. type: Plain text
msgid "None specific to this release."
msgstr "Aucun spécifique à cette version."

#. type: Plain text
msgid "See the list of [[long-standing issues|support/known_issues]]."
msgstr ""
"Voir la liste des [[problèmes connus de longue date|support/known_issues]]."

#. type: Title #
#, no-wrap
msgid "Get Tails 4.7"
msgstr ""

#. type: Title ##
#, no-wrap
msgid "To upgrade your Tails USB stick and keep your persistent storage"
msgstr ""
"Pour mettre à jour votre clé USB Tails et conserver votre stockage persistant"

#. type: Plain text
msgid "- Automatic upgrades are available from Tails 4.2 or later to 4.7."
msgstr ""

#. type: Bullet: '- '
msgid ""
"If you cannot do an automatic upgrade or if Tails fails to start after an "
"automatic upgrade, please try to do a [[manual upgrade|doc/upgrade/#manual]]."
msgstr ""
"Si vous ne pouvez pas faire une mise à jour automatique ou si le démarrage "
"de Tails échoue après une mise à jour automatique, merci d'essayer de faire "
"une [[mise à jour manuelle|doc/upgrade/#manual]]."

#. type: Title ##
#, no-wrap
msgid "To install Tails on a new USB stick"
msgstr "Pour installer Tails sur une nouvelle clé USB"

#. type: Plain text
msgid "Follow our installation instructions:"
msgstr "Suivez nos instructions d'installation :"

#. type: Bullet: '  - '
msgid "[[Install from Windows|install/win]]"
msgstr "[[Installer depuis Windows|install/win]]"

#. type: Bullet: '  - '
msgid "[[Install from macOS|install/mac]]"
msgstr "[[Installer depuis macOS|install/mac]]"

#. type: Bullet: '  - '
msgid "[[Install from Linux|install/linux]]"
msgstr "[[Installer depuis Linux|install/linux]]"

#. type: Plain text
#, no-wrap
msgid "<div class=\"caution\"><p>All the data on this USB stick will be lost.</p></div>\n"
msgstr ""
"<div class=\"caution\"><p>Toutes les données sur cette clé USB seront "
"perdues.</p></div>\n"

#. type: Title ##
#, no-wrap
msgid "To download only"
msgstr "Pour seulement télécharger"

#. type: Plain text
msgid ""
"If you don't need installation or upgrade instructions, you can download "
"Tails 4.7 directly:"
msgstr ""

#. type: Bullet: '  - '
msgid "[[For USB sticks (USB image)|install/download]]"
msgstr "[[Pour clés USB (image USB)|install/download]]"

#. type: Bullet: '  - '
msgid "[[For DVDs and virtual machines (ISO image)|install/download-iso]]"
msgstr "[[Pour DVD et machines virtuelles (image ISO)|install/download-iso]]"

#. type: Title #
#, no-wrap
msgid "What's coming up?"
msgstr "Et ensuite ?"

#. type: Plain text
msgid "Tails 4.8 is [[scheduled|contribute/calendar]] for June 30."
msgstr ""

#. type: Plain text
msgid "Have a look at our [[!tails_roadmap]] to see where we are heading to."
msgstr ""
"Jetez un œil à notre [[feuille de route|contribute/roadmap]] pour savoir ce "
"que nous avons en tête."

#. type: Plain text
#, no-wrap
msgid ""
"We need your help and there are many ways to [[contribute to\n"
"Tails|contribute]] (<a href=\"https://tails.boum.org/donate/?r=4.7\">donating</a> is only one of\n"
"them). Come [[talk to us|about/contact#tails-dev]]!\n"
msgstr ""
